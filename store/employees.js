const moment = require('moment');

export const state = () => ({
  currentID: 123,
  employees: [
    {
      firstName: 'Test Guy',
      id: '123'
    }
  ],
  activeEmployee: {}
});

export const mutations = {
  addEmployee(state, name) {
    state.employees.data.push({
      name: name
    });
  },
  setID(state, inputID) {
    state.currentID = inputID;
  },
  setEmployees(state, employees) {
    state.employees = employees;
  },
  setActiveEmployee(state, employee) {
    state.activeEmployee = employee;
  }
};

export const getters = {
  getEmployee(state) {
    return state.employees[0].data.find(employee => {
      return `${employee.first_name}-${employee.last_name}` == state.currentID;
    });
  },
  getStartDate(state) {
    const today = moment();
    const startDate = moment(state.activeEmployee.start_date);
    const remaining = startDate.diff(today, 'days');
    return remaining;
  },
  getActiveEmployee(state) {
    return state.activeEmployee;
  }
};
